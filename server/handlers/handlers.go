package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/upchieve/two-am-takeout/server/sockets"
)

// HealthBody is the response body for a health check
type HealthBody struct {
	OK bool `json:"OK"`
}

// ServeHealth handles healthcheck requests
func ServeHealth(w http.ResponseWriter, r *http.Request) {
	//based on the error message from console, instead of http://localhost:8080 it should be http://localhost:8081
	w.Header().Set("Access-Control-Allow-Origin", "http://localhost:8081")
	json.NewEncoder(w).Encode(HealthBody{OK: true})
}

// ServeSocket returns a socket handler
func ServeSocket(h *sockets.Hub) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		sockets.ServeWs(h, w, r)
	}
}
