This file is for taking notes as you go.
1. There is a test for the backend that is failing, fix the code so it passes.

To find out where the problem is 
- When I run 'go test ./server -v', I got the message FAIL: TestServerSuite/TestHealthCheck. I went to main_test.go file but couldn't figure it out.

To get more info
- To find out the issue, I ran 'npm run serve' and 'go run ./server' and open the browser console. The error message was "Access to XMLHttpRequest at 'http://localhost:3000/health' from origin 'http://localhost:8081' has been blocked by CORS policy: The 'Access-Control-Allow-Origin' header has a value 'http://localhost:8080' that is not equal to the supplied origin."

Fixed the issue
- I found the issue is in function ServeHealth at handlers.go file. I changed "http://localhost:8080" into "http://localhost:8081"

1. There is a test for the backend where the assumptions about what needs to be tested is incorrect. The test passes, but doesn't guarantee compatibility with how the frontend expects to communicate. You need to think about how the two interact and are compatible, and change the test code to match.

To find out where the problem is 
- After fixing the first issue, the browser console showed "backend check failed, response code was 200, data was [object Object]". It's from Chat.vue file.

To get more info
- I used console.log(res) and found data: {OK: true}, the 'ok' should be capital letters.  

Fixed the issue
- Changed 'res.data.ok' into 'res.data.OK' and also, went to main_test.go file to and changed "\"ok\":true" into '\"OK\":true"


1. In the frontend, make it so the user can enter a name that gets sent with every message and displayed to other users.

- Added text input for user to type their name, and created a v-model called 'sender'.
- The initial value of sender is empty.
- Revised the sendMessage function. sendMessage: function(text)----> sendMessage: function(text, name)   
- Added {{message.name}} to display other users' name

### Bonus Points

1. There is a security issue in the socket server. Identify and fix it. (Hint, it's not a problem in the health check).
- In hamdlers.go line 18. "w.Header().Set("Access-Control-Allow-Origin", "http://localhost:8081")" ask to check oringin
- In client.go line 37. CheckOrigin wasn't doing anything and just returning true.
- Add condition in this function to check if the origin match with the client side "localhost:8081" or not set

1. Make the frontend look nicer.
-If name field or messgage field is empty, this function show a message to prevent name and message from being submitted